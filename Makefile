# basic run of the submission script with parameters from #SBATCH in script
test:
	sbatch test.sub

# showing use of command line parameters to override #SBATCH in script
nametest:
	sbatch -J newname test.sub

# tidy up the outputs so far. Use with care on a real system, or NOT AT ALL!
clean:
	rm -Ir Outputs/* Runs/*

